// TODO: Move this data to a server, so it can be synced on first launch

// sample output of server
localStorage.setItem('recipeData', JSON.stringify([
    {
        id: 0,
        title: "Berry Cake",
        description: "Delicious, accoding to Julien. Could also be used to make Muffins (18 of them actually, at 7cm diagonal)",
        image: "https://yummy.ant.lgbt/_recipes/berry-cake/image.jpg",
        ingredients: [
            '200g butter',
            '200g sugar',
            '0.25tl salt',
            '4 eggs',
            '2el lemon juice',
            '200g flour',
            '100g ground peeled almonds',
            '1tl baking powder',
            '250g raspberries',
            '125g blueberries'
        ],
        directions: [
            'Preheat oven at 180°C',
            'Put butter in a bowl, mix with sugar and salt',
            'Mix one egg at a time into that bowl and add lemon juice',
            'In another bowl, mix flour, almonds and baking powder. Then add to main bowl',
            'Slowly and carefully mix berries into the dough',
            'Pour into the pan and bake for 1h'
        ]
    },
    {
        id: 1,
        title: "Madeira Loaf-Cake",
        description: "A classic English sponge cake, delicately flavoured with lemon and almond - perfect for afternoon tea ",
        image: "https://yummy.ant.lgbt/_recipes/madeira-loaf-cake/image.jpg",
        ingredients: [
            '175g butter',
            '160g sugar',
            '3 eggs',
            'greated zest 1 lemon',
            'few drops vanilla extract',
            '200g flour',
            'about 1tl baking powder',
            '50g ground almond'
        ],
        directions: [
            'Preheat oven to 160°C',
            'Beat together the butter and sugar until light and creamy',
            'Beat in the eggs one at a time',
            'Add the lemon zest and vanilla',
            'Now beat in the flour, baking poweder and almonds (mixed together before seperately) until you have a thick batter. The batter should be loose enough that it falls off a wooden spoon, if it’s too thick mix in a splash of milk',
            'Butter and line the base of a 900g loaf tin with greaseproof paper',
            'Tip the batter into the tin and smooth over the top. Bake for 55 to 60 Minutes'
        ]
    }
]));