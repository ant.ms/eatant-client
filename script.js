if (localStorage.getItem('useLocalStorage') != true) {
    // call server and get data (first run)
    // TODO
    var recipeData = JSON.parse(localStorage.getItem('recipeData'));
}
else {
    // get data from localstorage
    var recipeData = JSON.parse(localStorage.getItem('recipeData'));
}

// #region Cards

output = "";

// take input data and work with it
recipeData.forEach(element => {
    output += `<div class="card" onclick="createPopup(${element.id})">
        <div class="cardLeft">
            <img src="${element.image}">
        </div>
        <div class="cardRight">
            <div class="cardtitle">
                ${element.title}
            </div>
            <div class="cardDescription">
                ${element.description}
            </div>
        </div></div>`;
});

page.innerHTML = output;

// #endregion

// #region Popup

function backToHub() {
    // toggle status of displayed thing
    recipePopup.style.display = 'none';
    pageContent.style.display = 'unset';
}

function createPopup(elementId) {
    // toggle status of displayed thing
    recipePopup.style.display = 'unset';
    pageContent.style.display = 'none';

    // get correct element out of dataset
    var element;
    recipeData.forEach(eventualElement => {
        if (eventualElement.id == elementId)
           element = eventualElement;
    });

    // header
    recipeImage.src = element.image;
    recipeTitle.innerText = element.title;
    recipeDescription.innerText = element.description;

    // ingredients
    output = "<h2>Ingredients</h2>";

    element.ingredients.forEach(ingredient => {
        output += `<p><label><input type="checkbox"><span>${ingredient}</span></label></p>`; // TODO: Add onclick to save check (also required load from local-data)
    });

    recipeIngredients.innerHTML = output;

    // directions
    output = "<h2>Directions</h2>";

    element.directions.forEach(direction => {
        output += `<p><label><input type="checkbox"><span>${direction}</span></label></p>`; // TODO: Add onclick to save check (also required load from local-data)
    });
    
    recipeDirections.innerHTML = output;
}
// #endregion

if('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('/service-worker.js')
             .then(function() { console.log("Service Worker Registered"); });
  }  