var cacheName = 'v1zzyy';
var lastCacheName = 'v0';

self.addEventListener('install', function (e) {
    e.waitUntil(
        caches.open(cacheName).then(function (cache) {
            return cache.addAll([
                '.',
                './_data.js',
                './index.html',
                './logo-beta-192.png',
                './logo-beta-512.png',
                './logo-beta.svg',
                './manifest.webmanifest',
                './script.js',
                './service-worker.js',
                './style.css'
            ]);
        })
    );
});

self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(function (cacheNames) {
            console.log('[ServiceWorker] activate', cacheName);
            return Promise.all(
                cacheNames.filter(function (cacheName) {
                    // Return true if you want to remove this cache,
                    // but remember that caches are shared across
                    // the whole origin
                }).map(function (cacheName) {
                    console.log('[ServiceWorker] activate delete', cacheName);
                    return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request, {
            ignoreSearch: true
        }).then(response => {
            return response || fetch(event.request);
        })
    );
});